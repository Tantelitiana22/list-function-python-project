import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="functional_list",  # Replace with your own username
    version="0.1.6",
    author="Andrianarivo Tantelitiana RAKOTOARIJAONA",
    author_email="tantelitiana22@gmail.com",
    description="package for map a list",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Tantelitiana22/list-function-python-project",
    packages=setuptools.find_packages(exclude=["tests", "tests.*"], where="src"),
    package_dir={'': 'src'},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
