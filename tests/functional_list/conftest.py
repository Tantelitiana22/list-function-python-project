import os
import sys

import pkg_resources

partial_path = os.path.join("..", "..", "src")
package_full_path = pkg_resources.resource_filename(__name__, partial_path)
sys.path.append(package_full_path)
